<?php

$server = "localhost";
$user = "root";
$password = "";
$nama_database = "eproc";

$db = mysqli_connect($server, $user, $password, $nama_database);
//mysqli_connect($server, $user, $password); 
//mysqli_select_db($db_name) or die ("koneksi GAGAL");
if( !$db ){
	die("Gagal terhubung dengan database: " . mysqli_connect_error());
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Pendaftaran PEGAWAI Baru | SMK Coding</title>
</head>

<body>
    <header>
        <h3>Siswa yang sudah mendaftar</h3>
    </header>

    <nav>
        <a href="form-daftar.php">[+] Tambah Baru</a>
    </nav>

    <br>

    <table border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $sql = "SELECT * FROM TBL_PEGAWAI";
        $query = mysqli_query($db, $sql);

        while($siswa = mysqli_fetch_array($query)){
            echo "<tr>";

            echo "<td>".$siswa['ID_PEG']."</td>";
            echo "<td>".$siswa['NAMA_PEG']."</td>";
            echo "<td>".$siswa['EMAIL_PEG']."</td>";
            echo "<td>";
            echo "<a href='form-edit.php?id=".$siswa['ID_PEG']."'>Edit</a> | ";
            echo "<a href='hapus.php?id=".$siswa['ID_PEG']."'>Hapus</a>";
            echo "</td>";
            echo "</tr>";
        }
        ?>

    </tbody>
    </table>

    <p>Total: <?php echo mysqli_num_rows($query) ?></p>

    </body>
</html>
